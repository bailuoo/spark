package com.xiaojiezhu.spark.rdd2;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.PairFunction;
import scala.Tuple2;

import java.util.ArrayList;
import java.util.List;

public class JavaMapToPair {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("app");
        JavaSparkContext sc = new JavaSparkContext(conf);

        List<String> strs = new ArrayList<>();
        strs.add("hello jane");
        strs.add("hello mark");
        strs.add("hello xmail");

        JavaRDD<String> rdd = sc.parallelize(strs);

        JavaPairRDD<Object, Object> pair = rdd.mapToPair(new PairFunction<String, Object, Object>() {
            @Override
            public Tuple2<Object, Object> call(String s) throws Exception {
                return new Tuple2<>(s.split(" ")[0], s);
            }
        });

        //lambda 表达式
        //JavaPairRDD<String[], String> pair1 = rdd.mapToPair(str -> new Tuple2<>(str.split(" "), str));

    }
}
