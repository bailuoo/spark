package com.xiaojiezhu.spark.rdd2

import org.apache.spark.{SparkConf, SparkContext}

object ScalaMapToPair {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)

    val rdd = sc.parallelize(List("hello a","hello b"));
    val pair = rdd.map(str => (str.split(" ")(0) , str))
  }
}
