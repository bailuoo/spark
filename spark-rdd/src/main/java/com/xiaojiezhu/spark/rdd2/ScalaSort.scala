package com.xiaojiezhu.spark.rdd2

import org.apache.spark.{SparkConf, SparkContext}

import scala.reflect.ClassTag


/**
  * scala sort
  */
object ScalaSort {

  def main(args : Array[String]) : Unit={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)


    sort2(sc)
  }

  def sort2(sc : SparkContext): Unit ={
    val rdd = sc.parallelize(List((1,3),(2,6),(2,11),(6,1),(9,1)))
    val sorting = new Ordering[(Int,Int)]{
      override def compare(x: (Int, Int), y: (Int, Int)): Int = (x._1 + x._2) - (y._1 + y._2)
    }
    val result = rdd.sortBy(x=>x)(sorting,manifest)
    result.foreach(println(_))
  }


  /**
    * 对非字符排序
    * @param sc
    */
  def sort1(sc : SparkContext) : Unit={
    val rdd = sc.parallelize(List(("s",1),("a",2),("c",1),("d",2),("1",1),("?",9)))
    val ordering = new Ordering[(String,Int)] {
      override def compare(x: (String, Int), y: (String, Int)): Int = x._1.compare(y._1)
    }
    val result = rdd.sortBy(x => x)(ordering,manifest)
    result.foreach(println(_))
  }
}
