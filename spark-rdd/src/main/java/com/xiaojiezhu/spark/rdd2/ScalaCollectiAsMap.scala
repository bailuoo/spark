package com.xiaojiezhu.spark.rdd2

import org.apache.spark.{SparkConf, SparkContext}

object ScalaCollectiAsMap {

  def main(arg : Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)
    val rdd = sc.parallelize(List((1,2),(3,4),(3,6)))
    rdd.collectAsMap().foreach(println(_))
  }
}
