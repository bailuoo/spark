package com.xiaojiezhu.spark.setup3

import org.apache.spark.{SparkConf, SparkContext}

/**
  * 累加器
  */
object ScalaAccumulator {

  def main(args : Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)
    val rdd = sc.textFile("/home/zxj/data/test/all.txt")

    val count = sc.longAccumulator("txtCount")
    val result = rdd.map(x => {
      if(x.equals("")){
        //累加器+1
        count.add(1)
      }
      x
    })

    //一定要执行保存，不然rdd的计算是惰性的，不会计算数据
    result.saveAsTextFile("/home/zxj/data/test/result2")

    println("count:" + count.value)
  }
}
