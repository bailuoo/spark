package com.xiaojiezhu.spark.setup3

import org.apache.spark.{SparkConf, SparkContext}

object ScalaPartition {

  def main(args : Array[String]) : Unit={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)

    val data = List((3,6),(5,4),(6,2))

    val rdd = sc.parallelize(data,2)

    var sum: Int = 0
    rdd.foreach(x => {
      println((x._1,x._2) + "=" + sum)
      sum += x._2
    })
  }

}
