package com.xiaojiezhu.spark.rdd.action

import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author 朱小杰
  *         时间 2017-11-26 .10:54
  *         说明 ...
  */
object ScalaInterSection {

  def main(args:Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)

    val rdd1 = sc.parallelize(List(1,2,3,3))
    val rdd2 = sc.parallelize(List(2,3,4))

    val result = rdd1.intersection(rdd2)

    result.collect().foreach(println)
  }
}
