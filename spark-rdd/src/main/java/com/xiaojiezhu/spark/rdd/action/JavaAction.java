package com.xiaojiezhu.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.PairFunction;
import scala.Function1;
import scala.Tuple2;

import java.util.Arrays;
import java.util.Date;

/**
 * @Author 朱小杰
 * 时间 2017-09-24 .17:21
 * 说明 ...
 */
public class JavaAction {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("app Name");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<String> r1 = sc.parallelize(Arrays.asList("a", "b", "c", "a", "a", "b"));

        String reduce = r1.reduce(new Function2<String, String, String>() {
            @Override
            public String call(String v1, String v2) throws Exception {

                return v1 + v2;
            }
        });

        System.out.println(reduce);
        String fold = r1.fold("朱小杰", new Function2<String, String, String>() {
            @Override
            public String call(String v1, String v2) throws Exception {
                return v1 + v2;
            }
        });
        System.out.println(fold);



    }
}
