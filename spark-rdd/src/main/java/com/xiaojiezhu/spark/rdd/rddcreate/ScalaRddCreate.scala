package com.xiaojiezhu.spark.rdd.rddcreate

import org.apache.spark.{SparkConf, SparkContext}

/**
  *  朱小杰
  *  时间 2017-09-24 .16:46
  *  说明 ...
  */
object ScalaRddCreate {

  def main(args : Array[String]) : Unit={
    val conf = new SparkConf().setMaster("local").setAppName("App name")
    val sc = new SparkContext(conf);

    val fileRdd = sc.textFile("filePath")

    val b = sc.parallelize(Seq("a","b","c"))
  }
}
