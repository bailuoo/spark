package com.xiaojiezhu.spark.rdd.action

import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author 朱小杰
  *         时间 2017-11-29 .23:38
  *         说明 ...
  */
object ScalaTop {

  def main(args: Array[String]): Unit = {
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)
    val rdd = sc.parallelize(List(1,2,3,3))

    val result = rdd.top(2)
    result.foreach(print)

    println("===================")
    val textRdd = sc.textFile("d:/text.txt")
    textRdd.top(2).foreach(println)
  }
}
