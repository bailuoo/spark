package com.xiaojiezhu.spark.rdd.init

import org.apache.spark.{SparkConf, SparkContext}

/**
  * 朱小杰 <br>
  * 时间 2017-09-23 .22:08<br>
  * 说明 使用scala代码初始化SparkContext <br>
  * setMaster()  设置集群url，local这个特殊值可以运行在单机线程中而无需连接集群<br>
  * setAppName() 设置应用的名称，使得可在集群管理器中可以找到这个应用的名称<br>
  */
object ScalaSparkContextInit {

  def main(args:Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("my app")
    val sc = new SparkContext(conf)
  }
}
