package com.xiaojiezhu.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * @author 朱小杰
 * 时间 2017-11-30 .22:10
 * 说明 ...
 */
public class JavaTakeOrder {

    /**
     * 必须这样写啊，要实现序列化接口<br>
     * 写匿名内部类，或者lambda表达式都会报错
     */
    static class Xc implements Comparator<Integer>,Serializable{
        @Override
        public int compare(Integer o1, Integer o2) {
            //从小到在提取
            return o1 - o2;
        }
    }

    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("app");
        JavaSparkContext sc = new JavaSparkContext(conf);
        JavaRDD<Integer> rdd = sc.parallelize(Arrays.asList(1,6, 2, 3, 4,2));
        List<Integer> result = rdd.takeOrdered(2, new Xc());

        result.forEach(i -> System.out.println(i));
    }
}
