package com.xiaojiezhu.spark.rdd.init;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;

/**
 * @Author 朱小杰
 * 时间 2017-09-23 .22:23
 * 说明 使用scala代码初始化SparkContext <br>
 *     setMaster()  设置集群url，local这个特殊值可以运行在单机线程中而无需连接集群<br>
 *     setAppName() 设置应用的名称，使得可在集群管理器中可以找到这个应用的名称<br>
 */
public class JavaSparkContextInit {

    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("my App");
        JavaSparkContext jsc = new JavaSparkContext(conf);
    }
}
