package com.xiaojiezhu.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

/**
 * @author 朱小杰
 * 时间 2017-11-26 .19:32
 * 说明 ...
 */
public class JavaReduce {

    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("app");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<Integer> rdd = sc.parallelize(Arrays.asList(1, 2, 3));
        Integer reduce = rdd.reduce((x, y) -> x + y);

        Integer fold = rdd.fold(1, (x, y) -> x + y);

        System.out.println(reduce);
        System.out.println(fold);
    }
}
