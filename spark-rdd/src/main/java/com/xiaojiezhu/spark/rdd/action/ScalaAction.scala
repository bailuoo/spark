package com.xiaojiezhu.spark.rdd.action

import org.apache.spark.{SparkConf, SparkContext}

import scala.reflect.ClassTag

/**
  * 朱小杰
  *         时间 2017-09-24 .18:26
  *         说明 ...
  */
object ScalaAction {

  def main(args : Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    var sc = new SparkContext(conf)

    val rdd = sc.parallelize(Seq(1,2,3,4))
    val a = rdd.aggregate((0,0))(
      (acc,value) => (acc._1 + value,acc._2 + 1),
      (acc1,acc2) => (acc1._1 + acc2._1,acc1._2 + acc2._2)
    )

    println(a)


    val t = rdd.aggregate((0,0))(
      (u,v) => (u._1 +v,u._2+1),
      (u1,u2) =>(u1._1 + u2._1,u1._2 + u2._2)
    )

    println(t)
  }




}
