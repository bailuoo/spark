package com.xiaojiezhu.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;

import java.util.Arrays;

/**
 * @author 朱小杰
 * 时间 2017-11-26 .12:20
 * 说明 ...
 */
public class JavaSubtract {
    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("app");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<Integer> rdd1 = sc.parallelize(Arrays.asList(1, 2, 3, 4, 5));
        JavaRDD<Integer> rdd2 = sc.parallelize(Arrays.asList(1, 2, 3));

        JavaRDD<Integer> result = rdd1.subtract(rdd2);
        result.collect().forEach(System.out::println);
    }
}
