package com.xiaojiezhu.spark.rdd.action

import org.apache.spark.{SparkConf, SparkContext}

/**
  *  朱小杰
  *         时间 2017-11-30 .22:07
  *         说明 ...
  */
object ScalaTakeOrder {

  def main(args : Array[String]) : Unit={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)
    val rdd = sc.parallelize(List(6,1,2,8,3,4))

    val a = new Ordering[Int]() {
      override def compare(x: Int, y: Int) = x -y
    }
    val result = rdd.takeOrdered(2)(a)
    result.foreach(println)

  }
}
