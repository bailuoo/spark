package com.xiaojiezhu.spark.rdd.action

import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author 朱小杰
  *         时间 2017-11-26 .12:26
  *         说明 ...
  */
object ScalaSubtract {

  def main(arg:Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)

    val  rdd1 = sc.parallelize(List(1,2,3,4,5))
    val  rdd2 = sc.parallelize(List(1,2,3))

    val result = rdd1.subtract(rdd2)
    result.collect().foreach(println)
  }
}
