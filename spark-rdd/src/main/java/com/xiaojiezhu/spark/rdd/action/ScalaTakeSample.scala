package com.xiaojiezhu.spark.rdd.action

import org.apache.spark.{SparkConf, SparkContext}

/**
  * 作者 朱小杰
  * 时间 2017-12-03 .16:51
  * 说明 ...
  */
object ScalaTakeSample {

  def main(args : Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)
    val rdd = sc.parallelize(List(1,2,3,3))
    val array = rdd.takeSample(false,2)

    println(array)
  }
}
