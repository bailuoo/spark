package com.xiaojiezhu.spark.rdd.wordcount

import org.apache.spark.{SparkConf, SparkContext}

/**
  * 朱小杰
  * 时间 2017-09-24 .9:32
  * 说明 ...
  */
object ScalaWordCount {

  def main(args : Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("wordcount")
    val sc = new SparkContext(conf)
    val dir = "G:\\javacode\\workspace\\spark\\spark-rdd\\src\\main\\java\\com\\xiaojiezhu\\spark\\rdd\\wordcount\\"
    val input = sc.textFile(dir + "wordcount.txt")
    val words = input.flatMap(line => line.split(" "))
    val counts = words.map(word => (word,1)).reduceByKey((x,y) => x + y)
    counts.saveAsTextFile(dir + "result")
  }
}
