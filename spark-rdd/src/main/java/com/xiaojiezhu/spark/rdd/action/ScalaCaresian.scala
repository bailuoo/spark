package com.xiaojiezhu.spark.rdd.action

import org.apache.spark.{SparkConf, SparkContext}

/**
  * @author 朱小杰
  *         时间 2017-11-26 .12:48
  *         说明 ...
  */
object ScalaCaresian {

  def main(arg : Array[String]): Unit ={
    val conf = new SparkConf().setMaster("local").setAppName("app")
    val sc = new SparkContext(conf)

    val rdd1 = sc.parallelize(List(1,2,3))
    val rdd2 = sc.parallelize(List(4,5,6))

    val result = rdd1.cartesian(rdd2)
    result.foreach(f => println(f._1 + "," + f._2))
  }
}
