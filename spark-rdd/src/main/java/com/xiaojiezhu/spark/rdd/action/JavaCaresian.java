package com.xiaojiezhu.spark.rdd.action;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import scala.Tuple2;

import java.util.Arrays;
import java.util.function.Consumer;

/**
 * @author 朱小杰
 * 时间 2017-11-26 .12:36
 * 说明 迪卡尔乘积
 */
public class JavaCaresian {

    public static void main(String[] args) {
        SparkConf conf = new SparkConf().setMaster("local").setAppName("app");
        JavaSparkContext sc = new JavaSparkContext(conf);

        JavaRDD<Integer> rdd1 = sc.parallelize(Arrays.asList(1, 2, 3));
        JavaRDD<Integer> rdd2 = sc.parallelize(Arrays.asList(4,5,6));

        JavaPairRDD<Integer, Integer> result = rdd1.cartesian(rdd2);

        result.collect().forEach(new Consumer<Tuple2<Integer, Integer>>() {
            @Override
            public void accept(Tuple2<Integer, Integer> t) {
                System.out.println(t._1() +  "," + t._2());
            }
        });
    }
}
