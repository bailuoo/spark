package com.xiaojiezhu.scala

/**
  * @author 朱小杰
  *         时间 2017-11-30 .22:59
  *         说明 ...
  */
object ImplictDemo {

  def main(args: Array[String]): Unit = {
    display("1")
    display(1)
    val jie = abc("jie")
    println(jie)
  }

  def display(value : String): Unit ={
    println(value)
  }

  def abc(value : String) : String = value + "13"

  implicit def typeOfConvetor111(value : Int): String = value.toString

}
